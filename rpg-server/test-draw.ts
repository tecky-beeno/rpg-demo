import { MonsterService } from './monster.service'

let monsterService = new MonsterService()

let one = 0
let two = 0
let five = 0
let total = 0

for (;;) {
  let { item_id } = monsterService.fight(1)
  if (item_id == 1) {
    one++
  } else if (item_id == 2) {
    two++
  } else if (item_id == 5) {
    five++
  } else {
    throw new Error('unknown item: ' + item_id)
  }
  total++
  // console.log(one, two, five, total)
  console.log({
    1: one / total, // 0.03864734299516908
    2: two / total, // 0.4782608695652174
    5: five / total, // 0.4830917874396135
    total,
  })
}

// 8:99:100
