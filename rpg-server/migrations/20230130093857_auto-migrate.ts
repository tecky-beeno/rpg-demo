import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('item'))) {
    await knex.schema.createTable('item', table => {
      table.increments('id')
      table.integer('monster_id').unsigned().notNullable().references('monster.id')
      table.text('name').notNullable()
      table.integer('quantity').notNullable()
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('item')
}
