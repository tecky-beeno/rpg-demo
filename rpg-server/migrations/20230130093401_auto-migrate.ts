import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
  await knex.raw('alter table `monster` drop column `quantity`')
}


export async function down(knex: Knex): Promise<void> {
  await knex.raw('alter table `monster` add column `quantity` integer not null')
}
