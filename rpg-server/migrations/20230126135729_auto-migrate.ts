import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('monster'))) {
    await knex.schema.createTable('monster', table => {
      table.increments('id')
      table.text('code').notNullable().unique()
      table.text('name').notNullable()
      table.integer('cost').notNullable()
      table.integer('quantity').notNullable()
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('monster')
}
