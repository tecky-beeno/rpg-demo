import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('history'))) {
    await knex.schema.createTable('history', table => {
      table.increments('id')
      table.integer('item_id').unsigned().notNullable().references('item.id')
      table.timestamps(false, true)
    })
  }
}


export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('history')
}
