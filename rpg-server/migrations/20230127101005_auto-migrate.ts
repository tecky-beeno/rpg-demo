import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex('monster').delete()
  await knex.raw('alter table `monster` add column `image` text not null')
}

export async function down(knex: Knex): Promise<void> {
  await knex.raw('alter table `monster` drop column `image`')
}
