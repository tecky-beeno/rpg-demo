import cors from 'cors'
import express, { NextFunction, Request, Response } from 'express'
import { print } from 'listening-on'
import { MonsterController } from './monster.controller'
import { MonsterService } from './monster.service'

let app = express()

app.use(cors())
app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

let monsterService = new MonsterService()

let monsterController = new MonsterController(monsterService)
app.use(monsterController.router)

app.use((error: any, req: Request, res: Response, next: NextFunction) => {
  res
    .status(error.statusCode || 500)
    .json({
      error: String(error).replace('TypeError: ', '').replace('Error: ', ''),
    })
})

app.use((req, res, next) => {
  res.status(404).json({ error: 'route not matched' })
})

let port = 8100
app.listen(port, () => {
  print(port)
})
