import { proxySchema } from 'better-sqlite3-proxy'
import { db } from './db'

export type Monster = {
  id?: number | null
  code: string
  name: string
  cost: number
  image: string
}

export type Item = {
  id?: number | null
  monster_id: number
  monster?: Monster
  name: string
  quantity: number
}

export type History = {
  id?: number | null
  item_id: number
  item?: Item
}

export type DBProxy = {
  monster: Monster[]
  item: Item[]
  history: History[]
}

export let proxy = proxySchema<DBProxy>({
  db,
  tableFields: {
    monster: [],
    item: [
      /* foreign references */
      ['monster', { field: 'monster_id', table: 'monster' }],
    ],
    history: [
      /* foreign references */
      ['item', { field: 'item_id', table: 'item' }],
    ],
  },
})
