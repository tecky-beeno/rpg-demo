import { NextFunction, Request, Response, Router } from 'express'

export class HttpController {
  router = Router()

  wrapMethod(method: (req: Request) => any) {
    return async (req: Request, res: Response, next: NextFunction) => {
      try {
        let json = await method(req)
        res.json(json)
      } catch (error) {
        next(error)
      }
    }
  }
}
