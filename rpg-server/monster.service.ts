import { filter, unProxy } from 'better-sqlite3-proxy'
import { db } from './db'
import { HttpError } from './http.error'
import { Item, Monster, proxy } from './proxy'

export class MonsterService {
  create(monster: Monster) {
    // await this.knex('monster').insert(monster).returning('id')
    let id = proxy.monster.push(monster)
    return { id }
  }

  list(search?: string) {
    let monsters = db
      .prepare(
        /* sql */ `
with monster_list as (
select
  monster.id
, monster.cost
, monster.name
, monster.code
, monster.image
, ifnull(sum(item.quantity),0) quantity
from monster
left join item on item.monster_id = monster.id
where monster.name like :search
   or monster.code like :search
group by monster.id
)
, used_list as (
select
  monster.id
, count(history.id) as used
from monster
left join item on item.monster_id = monster.id
left join history on history.item_id = item.id
group by monster.id
)

select
  monster_list.*
, (monster_list.quantity - used_list.used) as remind
from monster_list
left join used_list on used_list.id = monster_list.id
`,
      )
      .all({
        search: '%' + (search || '') + '%',
      })
    return { monsters }
  }

  getDetail(code: string) {
    let monster = db
      .prepare(
        /* sql */ `
select
  id
, cost
, name
, code
, image
from monster
where code = :code
`,
      )
      .get({ code })
    if (!monster) {
      throw new HttpError(404, 'monster not found')
    }
    let items = filter(proxy.item, { monster_id: monster.id }).map(unProxy)
    return { monster, items }
  }

  update(
    monster_id: number,
    monster: Monster,
    items: {
      id: number
      name: string
      quantity: number
    }[],
  ) {
    // await this.knex.update(monster).from('monster').where({ id })
    proxy.monster[monster_id] = monster
    for (let item of items) {
      if (item.id) {
        proxy.item[item.id].name = item.name
        // TODO check quantity
        proxy.item[item.id].quantity = item.quantity
      } else {
        proxy.item.push({
          monster_id: monster_id,
          name: item.name,
          quantity: item.quantity,
        })
      }
    }
    return { id: monster_id }
  }

  fight(monster_id: number) {
    // let monster = proxy.monster[monster_id]

    let items = db
      .prepare(
        /* sql */ `
select
  item.id
, (item.quantity - count(history.id)) as remain
from item
left join history on history.item_id = item.id
where item.monster_id = :monster_id
group by item.id
having remain > 0
`,
      )
      .all({ monster_id })

    let total = 0

    for (let item of items) {
      total += item.remain
    }

    // console.log('items:', items)
    // console.log('total:', total)

    if (total < 1) {
      throw new HttpError(503, 'no items available from this monster')
    }

    let dice = Math.random() * total
    let last = 0

    let item_id: number | undefined
    for (let item of items) {
      let next = last + item.remain
      if (dice < next) {
        item_id = item.id
        break
      }
      last = next
    }
    if (!item_id) {
      throw new Error('logic error?')
    }

    proxy.history.push({
      item_id,
    })

    let item = proxy.item[item_id]

    return {
      item: {
        id: item_id,
        name: item.name,
      },
    }
  }
}
