import { proxy } from './proxy'

proxy.monster[1] = {
  name: 'Apple',
  code: 'APL001',
  cost: 3,
  image: 'https://picsum.photos/seed/apple/1080/1080',
}
proxy.item[1] = {
  monster_id: 1,
  name: 'Apple Pie',
  quantity: 10,
}
proxy.item[2] = {
  monster_id: 1,
  name: 'Apple Juice',
  quantity: 100,
}
proxy.history[1] = {
  item_id: 1,
}
proxy.history[2] = {
  item_id: 1,
}
proxy.history[3] = {
  item_id: 2,
}

proxy.monster[2] = {
  name: 'Cherry',
  code: 'CHE001',
  cost: 30,
  image: 'https://picsum.photos/seed/cherry/1080/1080',
}
proxy.item[3] = {
  monster_id: 2,
  name: 'Cherry Pie',
  quantity: 10,
}
proxy.item[4] = {
  monster_id: 2,
  name: 'Cherry Juice',
  quantity: 100,
}
proxy.history[4] = {
  item_id: 3,
}

console.log('ready')
