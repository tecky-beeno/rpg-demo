import { proxy } from './proxy'
import { filter } from 'better-sqlite3-proxy'

console.log('counting remain...')

for (let monster of proxy.monster) {
  let items = filter(proxy.item, { monster_id: monster.id! })
  let quantity = 0
  let used = 0
  for (let item of items) {
    quantity += item.quantity

    // let histories = filter(proxy.history, { item_id: item.id! })
    // used += histories.length

    used += filter(proxy.history, { item_id: item.id! }).length
  }
  let remain = quantity - used
  console.log({
    id: monster.id,
    name: monster.name,
    quantity,
    used,
    remain,
  })
}
