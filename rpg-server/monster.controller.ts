import express, { NextFunction, Request, Response } from 'express'
import { HttpController } from './http.controller'
import { MonsterService } from './monster.service'
import { array, id, int, object, optional, string } from 'cast.ts'
import { Formidable } from 'formidable'
import fs from 'fs'
import { Monster } from './proxy'
import { HttpError } from './http.error'

let uploadDir = 'uploads'
fs.mkdirSync(uploadDir, { recursive: true })

export class MonsterController extends HttpController {
  constructor(private monsterService: MonsterService) {
    super()

    let form = new Formidable({
      uploadDir,
      keepExtensions: true,
      filter: part => !!part.mimetype?.startsWith('image/'),
    })

    let upload = (req: Request, res: Response, next: NextFunction) => {
      form.parse(req, (err, fields, files) => {
        if (err) {
          next(err)
          return
        }
        req.body = { fields, files }
        next()
      })
    }

    this.router.post('/monsters', upload, this.wrapMethod(this.create))
    this.router.use('/uploads', express.static(uploadDir))
    this.router.get('/monsters', this.wrapMethod(this.list))
    this.router.get('/monsters/:code', this.wrapMethod(this.detail))
    this.router.post('/monsters/:id', upload, this.wrapMethod(this.update))
    this.router.post('/monsters/:id/fight', this.wrapMethod(this.fight))
  }

  create = (req: Request) => {
    let parser = object({
      body: object({
        fields: object({
          code: string({ trim: true, minLength: 6, maxLength: 11 }),
          name: string({ trim: true, minLength: 3, maxLength: 20 }),
          cost: int({ min: 0 }),
        }),
        files: object({
          image: object({
            newFilename: string({ trim: true, minLength: 6, maxLength: 50 }),
          }),
        }),
      }),
    })
    let body = parser.parse(req).body
    let monster: Monster = {
      code: body.fields.code,
      cost: body.fields.cost,
      image: body.files.image.newFilename,
      name: body.fields.name,
    }
    return this.monsterService.create(monster)
  }

  list = (req: Request) => {
    let parser = object({
      query: object({
        search: optional(string()),
      }),
    })
    let search = parser.parse(req).query.search
    return this.monsterService.list(search)
  }

  detail = (req: Request) => {
    let parser = object({
      params: object({
        code: string({ trim: true, minLength: 6, maxLength: 11 }),
      }),
    })
    let code = parser.parse(req).params.code
    return this.monsterService.getDetail(code)
  }

  update = (req: Request) => {
    // console.log('body:', req.body)
    let parser = object({
      params: object({
        id: id(),
      }),
      body: object({
        fields: object({
          code: string({ trim: true, minLength: 6, maxLength: 11 }),
          name: string({ trim: true, minLength: 3, maxLength: 20 }),
          cost: int({ min: 0 }),
          image: optional(string()),
          items: string({}),
        }),
        files: object({
          image: optional(
            object({
              newFilename: string({ trim: true, minLength: 6, maxLength: 50 }),
            }),
          ),
        }),
      }),
    })
    let { params, body } = parser.parse(req)
    let image = body.fields.image || body.files.image?.newFilename
    if (!image) {
      throw new HttpError(400, 'missing monster image')
    }
    let itemsParser = array(
      object({
        id: int(),
        name: string(),
        quantity: int(),
      }),
    )
    let items = itemsParser.parse(JSON.parse(body.fields.items))
    // console.log('items:', items)
    let monster: Monster = {
      code: body.fields.code,
      cost: body.fields.cost,
      image,
      name: body.fields.name,
    }
    return this.monsterService.update(params.id, monster, items)
  }

  fight = (req: Request) => {
    let parser = object({
      params: object({
        id: id(),
      }),
    })
    let { params } = parser.parse(req)
    return this.monsterService.fight(params.id)
  }
}
