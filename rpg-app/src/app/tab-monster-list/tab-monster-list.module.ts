import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabMonsterListPageRoutingModule } from './tab-monster-list-routing.module';

import { TabMonsterListPage } from './tab-monster-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabMonsterListPageRoutingModule
  ],
  declarations: [TabMonsterListPage]
})
export class TabMonsterListPageModule {}
