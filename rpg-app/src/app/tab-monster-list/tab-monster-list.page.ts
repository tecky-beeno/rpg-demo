import { ActivatedRoute } from '@angular/router'
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core'
import { ApiService } from '../api.service'
import { MonsterOverview } from '../monster'
import { mergeMap } from 'rxjs'

interface Egg {
  width: number
  height: number
  color: number
  position: {
    x: number
    y: number
  }
  degree: number
  rotation: number
  speed: {
    x: number
    y: number
  }
  scale: number
}

@Component({
  selector: 'app-tab-monster-list',
  templateUrl: './tab-monster-list.page.html',
  styleUrls: ['./tab-monster-list.page.scss'],
})
export class TabMonsterListPage implements OnInit {
  searchText = ''

  monsters: MonsterOverview[] = []

  autoLoad$ = this.route.params.pipe(mergeMap(() => this.loadMonsters()))

  isShowDraw = false

  constructor(private api: ApiService, private route: ActivatedRoute) {
    for (let i = 0; i < 10; i++) {
      this.eggs.push(this.newEgg())
    }
  }

  async ngOnInit() {}

  async loadMonsters() {
    let params = new URLSearchParams()
    params.set('search', this.searchText)
    let json = await this.api.get('/monsters?' + params)
    if (json.monsters) {
      this.monsters = json.monsters
    }
  }

  getImageUrl(monster: MonsterOverview) {
    return this.api.toImageUrl(monster.image)
  }

  async fight(monster: MonsterOverview) {
    this.isShowDraw = true
    this.start(monster)
  }

  /* drawing modal */

  @ViewChild('diceAreaRef')
  diceAreaRef?: ElementRef<HTMLDivElement>

  @ViewChild('outputRef')
  outputRef?: ElementRef<HTMLDivElement>

  shell = {
    width: 245,
    height: 200,
  }
  eggs: Egg[] = []
  output = {
    width: 60,
    height: 60,
  }
  droppingEgg?: Egg

  isRolling = false
  isDropping = false
  isCentering = false
  isShowResult = false
  resultItemName?: string

  newEgg(): Egg {
    return {
      width: 50,
      height: 50,
      color: Math.floor(Math.random() * 3) + 1,
      position: {
        x: this.shell.width / 2,
        y: this.shell.height / 2,
      },
      degree: 0,
      rotation: Math.random() * 2 - 1,
      speed: {
        x: Math.random() * 2 - 1,
        y: Math.random() * 2 - 1,
      },
      scale: 1,
    }
  }

  start(monster: MonsterOverview) {
    for (let egg of this.eggs) {
      egg.scale = 1
    }
    delete this.droppingEgg
    this.isRolling = true
    this.update(monster)
    setTimeout(() => this.drop(monster), 2000)
  }

  drop(monster: MonsterOverview) {
    this.isRolling = false
    this.isDropping = true
    let idx = Math.floor(Math.random() * this.eggs.length)
    let egg = this.eggs[idx]
    this.droppingEgg = egg
    egg.position.y = 0
    egg.position.x = this.output.width / 2
    this.update(monster)
  }

  update(monster: MonsterOverview) {
    if (this.isRolling) {
      for (let egg of this.eggs) {
        egg.degree += egg.rotation * 5

        let speedStep = 3
        egg.position.x += egg.speed.x * speedStep
        egg.position.y += egg.speed.y * speedStep

        if (egg.position.x < egg.width / 2) {
          egg.position.x = egg.width / 2
          egg.speed.x *= -1
        }
        if (egg.position.x > this.shell.width - egg.width / 2) {
          egg.position.x = this.shell.width - egg.width / 2
          egg.speed.x *= -1
        }

        if (egg.position.y < egg.height / 2) {
          egg.position.y = egg.height / 2
          egg.speed.y *= -1
        }
        if (egg.position.y > this.shell.height - egg.height / 2) {
          egg.position.y = this.shell.height - egg.height / 2
          egg.speed.y *= -1
        }
      }
    } else if (this.isDropping && this.droppingEgg) {
      let egg = this.droppingEgg
      egg.position.y++
      // egg.position.y += 0.2
      if (egg.position.y > this.output.height - egg.height / 2) {
        let rect = this.outputRef?.nativeElement.getBoundingClientRect()
        if (rect) {
          // debugger
          egg.position.x = rect.left + rect.width / 2
          egg.position.y = rect.top - egg.height / 2

          this.isDropping = false
          this.isCentering = true
        }
      }
    } else if (this.isCentering && this.droppingEgg) {
      let rect = this.diceAreaRef?.nativeElement.getBoundingClientRect()
      if (rect) {
        let cx = rect.width / 2
        let cy = rect.height / 2
        let cScale = 2

        let egg = this.droppingEgg

        let dx = cx - egg.position.x
        let dy = cy - egg.position.y
        let dScale = cScale - egg.scale

        let step = 0.05
        // let step = 0.01

        egg.position.x += dx * step
        egg.position.y += dy * step
        egg.scale += dScale * step

        // console.log({ dx, dy })

        if (Math.abs(dx) < 1 && Math.abs(dy) < 1) {
          this.isCentering = false
        }
      }
    } else {
      this.requestDraw(monster)
      return
    }

    requestAnimationFrame(() => this.update(monster))
  }

  async requestDraw(monster: MonsterOverview) {
    let json = await this.api.post(`/monsters/${monster.id}/fight`, {})
    console.log(json)
    if (json.item) {
      this.api.showSuccess('you got item ' + json.item.name)
      monster.remind--
      // setTimeout(() => {
      this.isShowDraw = false
      this.isShowResult = true
      this.resultItemName = json.item.name
      // }, 3500)
    }
  }

  closeResult() {
    this.isShowResult = false
    delete this.resultItemName
  }
}
