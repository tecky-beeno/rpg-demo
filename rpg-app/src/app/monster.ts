export interface NewMonster {
  code: string
  name: string
  cost: number
  image?: File
}

export interface MonsterOverview {
  id: number
  code: string
  name: string
  cost: number
  quantity: number
  remind: number
  image: string
}

export interface MonsterDetail {
  id: number
  code: string
  name: string
  cost: number
  // quantity: number
  // remind: number
  image: string
}
export interface MonsterItem {
  id: number
  name: string
  quantity: number
}
