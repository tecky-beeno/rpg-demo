import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateMonsterPage } from './create-monster.page';

const routes: Routes = [
  {
    path: '',
    component: CreateMonsterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateMonsterPageRoutingModule {}
