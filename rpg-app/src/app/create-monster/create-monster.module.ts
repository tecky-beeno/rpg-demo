import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateMonsterPageRoutingModule } from './create-monster-routing.module';

import { CreateMonsterPage } from './create-monster.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateMonsterPageRoutingModule
  ],
  declarations: [CreateMonsterPage]
})
export class CreateMonsterPageModule {}
