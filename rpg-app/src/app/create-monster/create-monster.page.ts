import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { ApiService } from '../api.service'
import { NewMonster } from '../monster'
import { selectImage } from '@beenotung/tslib/file'
import { compressMobilePhoto, dataURItoBlob } from '@beenotung/tslib/image'

@Component({
  selector: 'app-create-monster',
  templateUrl: './create-monster.page.html',
  styleUrls: ['./create-monster.page.scss'],
})
export class CreateMonsterPage implements OnInit {
  monster: NewMonster = {
    code: '',
    cost: 10,
    name: '',
  }
  imagePreview?: string

  constructor(private api: ApiService, private router: Router) {}

  ngOnInit() {}

  async submit() {
    let formData = new FormData()
    formData.set('code', this.monster.code)
    formData.set('name', this.monster.name)
    formData.set('cost', this.monster.cost.toString())
    if (this.monster.image) {
      formData.set('image', this.monster.image)
    }
    let json = await this.api.upload('/monsters', formData)
    if (json.id) {
      await this.api.showSuccess('Created Monster #' + json.id)
      this.router.navigateByUrl('/tabs/monster-list', { replaceUrl: true })
    }
  }

  async selectImage() {
    let files = await selectImage()
    let file = files[0]
    if (!file) return
    this.imagePreview = await compressMobilePhoto({ image: file })
    let blob = dataURItoBlob(this.imagePreview)
    file = new File([blob], file.name, {
      type: blob.type,
      lastModified: file.lastModified,
    })
    this.monster.image = file
  }
}
