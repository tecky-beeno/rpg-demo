import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ApiService } from '../api.service'
import { MonsterDetail, MonsterOverview, MonsterItem } from '../monster'
import { map } from 'rxjs/operators'
import { selectImage } from '@beenotung/tslib/file'
import { compressMobilePhoto, dataURItoBlob } from '@beenotung/tslib/image'

@Component({
  selector: 'app-edit-monster',
  templateUrl: './edit-monster.page.html',
  styleUrls: ['./edit-monster.page.scss'],
})
export class EditMonsterPage implements OnInit {
  code: string

  error?: string
  monster?: MonsterDetail
  items: MonsterItem[] = []
  imagePreview?: string
  imageFile?: File

  constructor(
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.code = this.route.snapshot.params['code']
  }

  async ngOnInit() {
    let json = await this.api.get('/monsters/' + this.code)
    this.error = json.error
    this.monster = json.monster
    this.items = json.items
  }

  async submit(monster: MonsterDetail) {
    let formData = new FormData()
    formData.set('code', monster.code)
    formData.set('name', monster.name)
    formData.set('cost', monster.cost.toString())
    if (this.imageFile) {
      formData.set('image', this.imageFile)
    } else {
      formData.set('image', monster.image)
    }
    formData.set('items', JSON.stringify(this.items))
    let json = await this.api.upload('/monsters/' + monster.id, formData)
    if (!json.error) {
      // this.router.navigateByUrl('/tabs/monster-list')
      this.api.showSuccess('saved monster details')
    }
  }

  async selectImage(monster: MonsterDetail) {
    let files = await selectImage()
    let file = files[0]
    if (!file) return
    this.imagePreview = await compressMobilePhoto({ image: file })
    let blob = dataURItoBlob(this.imagePreview)
    file = new File([blob], file.name, {
      type: blob.type,
      lastModified: file.lastModified,
    })
    this.imageFile = file
    monster.image = file.name
  }

  getImageUrl(monster: MonsterDetail) {
    return this.imagePreview || this.api.toImageUrl(monster.image)
  }

  addItem() {
    this.items.push({
      id: 0,
      name: '',
      quantity: 0,
    })
  }
}
