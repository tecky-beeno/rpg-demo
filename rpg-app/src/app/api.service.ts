import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { ToastController } from '@ionic/angular'

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  api_origin = 'http://localhost:8100'

  constructor(private toastController: ToastController) {}

  async get(url: string) {
    let res = await fetch(this.api_origin + url, {})
    let json = await res.json()
    if (json.error) {
      await this.showError(json)
    }
    return json
  }

  async post(url: string, body: object) {
    let res = await fetch(this.api_origin + url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
    let json = await res.json()
    if (json.error) {
      await this.showError(json)
    }
    return json
  }

  async upload(url: string, body: FormData) {
    let res = await fetch(this.api_origin + url, {
      method: 'POST',
      body,
    })
    let json = await res.json()
    if (json.error) {
      await this.showError(json)
    }
    return json
  }

  async patch(url: string, body: object) {
    let res = await fetch(this.api_origin + url, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    })
    let json = await res.json()
    if (json.error) {
      await this.showError(json)
    }
    return json
  }

  async showSuccess(message: string) {
    let toast = await this.toastController.create({
      message,
      duration: 3500,
      color: 'success',
      buttons: [
        {
          role: 'cancel',
          text: 'Dismiss',
          handler: () => {
            toast.dismiss()
          },
        },
      ],
    })
    await toast.present()
  }

  async showError(json: any) {
    let toast = await this.toastController.create({
      message: json.error,
      duration: 3800,
      color: 'danger',
      buttons: [
        {
          role: 'cancel',
          text: 'Dismiss',
          handler: () => {
            toast.dismiss()
          },
        },
      ],
    })
    await toast.present()
  }

  toImageUrl(path: string) {
    if (path.startsWith('http')) {
      return path
    }
    return this.api_origin + '/uploads/' + path
  }
}
