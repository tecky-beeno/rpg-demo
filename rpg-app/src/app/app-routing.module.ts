import { NgModule } from '@angular/core'
import { PreloadAllModules, RouterModule, Routes } from '@angular/router'

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./tabs/tabs.module').then(m => m.TabsPageModule),
  },
  {
    path: 'create-monster',
    loadChildren: () =>
      import('./create-monster/create-monster.module').then(
        m => m.CreateMonsterPageModule,
      ),
  },
  {
    path: 'edit-monster/:code',
    loadChildren: () =>
      import('./edit-monster/edit-monster.module').then(
        m => m.EditMonsterPageModule,
      ),
  },
]
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
